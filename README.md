# Keid - 2D tile maps

![2022-01-25](https://i.imgur.com/HGIG45k.png)

The pipeline is a simple yet efficient method to draw metric tons of tiles in one go.

It is achieved by using a texture where tileset positions are encoded in red and green channels of an ordinary map.
This requires some preparation to convert a map into texture, but then no additional setup is needed at runtime.

## Features

- Stages: 1
- Pipelines:
  * Unlit/TileMap
- Resources:
  * textures
