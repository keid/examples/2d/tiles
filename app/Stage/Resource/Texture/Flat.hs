{-# LANGUAGE DeriveAnyClass #-}

{-# OPTIONS_GHC -Wno-unused-top-binds #-}
-- XXX: the example uses shared textured and only needs some of them

module Stage.Resource.Texture.Flat
  ( Collection(..)
  , TextureCollection
  , sources
  , indices
  ) where

import RIO

import GHC.Generics (Generic1)

import Global.Resource.Texture.Base qualified as Base
import Resource.Collection (Generically1(..))
import Resource.Collection qualified as Collection
import Resource.Source (Source(..))
import Resource.Texture (Texture, Flat)
import Stage.Resource.TileMaps.Bitpack qualified as Bitpack
import Stage.Resource.TileMaps.LTTP qualified as LTTP
-- import Stage.Resource.TileMaps.Spelunky qualified as Spelunky

data Collection a = Collection
  { base :: Base.Collection a
  , bitpack :: Bitpack.Collection a
  , lttp :: LTTP.Collection a
  -- , spelunky :: Spelunky.Collection a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic1)
  deriving Applicative via (Generically1 Collection)

type TextureCollection = Collection (Texture Flat)

sources :: Collection Source
sources = Collection
  { base = Base.sources
  , bitpack = Bitpack.sources
  , lttp = LTTP.sources
  -- , spelunky = Spelunky.sources
  }

indices :: Collection Int32
indices = fmap fst $ Collection.enumerate sources
