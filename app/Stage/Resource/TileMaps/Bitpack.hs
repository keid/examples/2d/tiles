{-# LANGUAGE DeriveAnyClass #-}

{-# OPTIONS_GHC -Wno-unused-top-binds #-}
-- XXX: the example uses shared textured and only needs some of them

module Stage.Resource.TileMaps.Bitpack
  ( Collection(..)
  , TextureCollection
  , sources
  ) where

import RIO

import GHC.Generics (Generic1)
import RIO.FilePath ((</>))

import Resource.Collection (Generically1(..))
import Resource.Source (Source(..))
import Resource.Static as Static
import Resource.Texture (Texture, Flat)

data Collection a = Collection
  { tileset :: a
  , layer0 :: a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic1)
  deriving Applicative via (Generically1 Collection)

type TextureCollection = Collection (Int32, Texture Flat)

Static.filePatterns Static.Files ("resources" </> "tilemaps" </> "bitpack")

sources :: Collection Source
sources = Collection
  { tileset = File Nothing TILESET_KTX2
  , layer0 = File Nothing LAYER0_KTX2
  }
