module Stage.Render
  ( updateBuffers
  , recordCommands
  ) where

import RIO

import RIO.State (gets)
import Vulkan.Core10 qualified as Vk
import Vulkan.NamedType ((:::))

import Engine.Types qualified as Engine
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Worker qualified as Worker
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Scene
import Render.Draw qualified as Draw
import Render.ForwardMsaa qualified as ForwardMsaa
import Resource.Model.Observer qualified as Model

import Stage.Types (FrameResources(..), RunState(..))

updateBuffers
  :: RunState
  -> FrameResources
  -> Basic.StageFrameRIO FrameResources RunState ()
updateBuffers RunState{..} FrameResources{..} = do
  Scene.observe rsSceneP frScene
  Model.observeCoherent rsViewportP frTiles

recordCommands
  :: Vk.CommandBuffer
  -> FrameResources
  -> "image index" ::: Word32
  -> Basic.StageFrameRIO FrameResources RunState ()
recordCommands cb FrameResources{..} imageIndex = do
  (_context, Engine.Frame{fSwapchainResources, fRenderpass, fPipelines}) <- ask

  texturedQuad <- gets rsQuadUV
  tiles <- Worker.readObservedIO frTiles

  ForwardMsaa.usePass (Basic.rpForwardMsaa fRenderpass) imageIndex cb do
    Swapchain.setDynamicFullscreen cb fSwapchainResources

    Scene.withBoundSet0 frScene (Basic.pUnlitTexturedBlend fPipelines) cb do
      Graphics.bind cb (Basic.pTileMap fPipelines) do
        Draw.indexed cb texturedQuad tiles
