module Stage.Setup
  ( stackStage
  , Stage
  , stage
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Engine.Camera qualified as Camera
import Engine.Types qualified as Engine
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Vulkan.Types (HasSwapchain, Queues)
import Engine.Worker qualified as Worker
import Geometry.Quad qualified as Quad
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Scene
import Render.Samplers qualified as Samplers
import Resource.CommandBuffer (withPools)
import Resource.Model qualified as Model
import Resource.Model.Observer qualified as Model (newCoherent)
import Resource.Region qualified as Region
import Resource.Texture.Ktx2 qualified as Ktx2
import RIO.State (gets)
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Stage.Render qualified as Render
import Stage.Resource.Texture.Flat qualified as Flat
import Stage.Types (FrameResources(..), RunState(..), Stage)
import Stage.World.Viewport qualified as Viewport

stackStage :: Engine.StackStage
stackStage = Engine.StackStage stage

stage :: Stage
stage = Engine.Stage
  { sTitle = "Main"

  , sInitialRS  = initialRunState
  , sAllocateRP = Basic.allocate_
  , sAllocateP  = allocatePipelines
  , sInitialRR  = intialRecyclableResources
  , sBeforeLoop = pure ()

  , sUpdateBuffers  = Render.updateBuffers
  , sRecordCommands = Render.recordCommands

  , sAfterLoop = pure
  }

allocatePipelines
  :: HasSwapchain swapchain
  => swapchain
  -> Basic.RenderPasses
  -> ResourceT (Engine.StageRIO RunState) Basic.Pipelines
allocatePipelines swapchain renderpasses = do
  -- Allocate static set of common samplers.
  samplers <- Samplers.allocate (Swapchain.getAnisotropy swapchain)

  -- Configure descriptors for statically-known set of flat textures, no cubes, and no shadows.
  let sceneBinds = Scene.mkBindings samplers Flat.sources Nothing 0

  Basic.allocatePipelines sceneBinds (Swapchain.getMultisample swapchain) renderpasses

initialRunState :: Engine.StageSetupRIO (Resource.ReleaseKey, RunState)
initialRunState = withPools \pools -> Region.run do
  ortho <- Camera.spawnOrthoPixelsCentered
  rsSceneP <- Worker.spawnMerge1 mkScene ortho

  logInfo "Loading models"
  rsQuadUV <- Model.createStagedL (Just "rsQuadUV") pools (Quad.toVertices Quad.texturedQuad) Nothing
  Model.registerIndexed_ rsQuadUV

  logInfo "Loading textures"
  rsTextures <- traverse (Ktx2.load pools) Flat.sources

  rsViewportP <- Viewport.spawn rsTextures

  pure RunState{..}
  where
    mkScene Camera.Projection{..} = Scene.emptyScene
      { Scene.sceneProjection = projectionTransform
      }

intialRecyclableResources
  :: Queues Vk.CommandPool
  -> Basic.RenderPasses
  -> Basic.Pipelines
  -> ResourceT (Engine.StageRIO RunState) FrameResources
intialRecyclableResources _cmdPools _renderPasses pipelines = do
  textures <- gets rsTextures
  frScene <- Scene.allocate
    (Basic.getSceneLayout pipelines)
    textures
    Nothing
    Nothing
    mempty
    Nothing

  frTiles <- Model.newCoherent 2 "Tiles"

  pure FrameResources{..}
