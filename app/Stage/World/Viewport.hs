{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedRecordDot #-}

module Stage.World.Viewport
  ( spawn
  , Process
  ) where

import RIO

import UnliftIO.Resource (MonadResource)
import Engine.Worker qualified as Worker
import Geomancy (ivec4, vec2)
import Geomancy.Transform qualified as Transform
import Render.Samplers qualified as Samplers
import Render.Unlit.TileMap.Model qualified as TileMap
import Resource.Texture qualified as Texture

import Stage.Resource.Texture.Flat qualified as Flat
import Stage.Resource.TileMaps.Bitpack qualified as Bitpack

type Process = Worker.Timed () TileMap.Stores

pattern FIXED_TIMESTEP :: (Eq a, Num a) => a
pattern FIXED_TIMESTEP = 120

spawn
  :: ( MonadResource m
     , MonadUnliftIO m
     )
  => Flat.Collection (Texture.Texture a)
  -> m Process
spawn textures =
  Worker.spawnTimed
    True
    (Left dt)
    initF
    stepF
    initialConfig
  where
    dt = 1e6 `div` FIXED_TIMESTEP

    initialConfig = ()
    vpWidth = 512
    vpHeight = 512
    tileSize = 16
    bitpackTextureIds = ivec4
      (Flat.indices.bitpack.tileset)
      (Samplers.indices.nearest)
      (Flat.indices.bitpack.layer0)
      0

    initF _config =
      pure
        ( TileMap.Attrs mempty mempty
        , 0
        )

    stepF state _config =
      pure
        ( Just $ mkViewport state
        , state + 1 / FIXED_TIMESTEP
        )

    mkViewport state = TileMap.Attrs [a, b] staticTransforms
      where
        a = tmpBase
          { TileMap.tmpViewOffset =
              aCenter + vec2 (sin aTime) (cos aTime) * aRadius
          , TileMap.tmpViewportSize =
              vec2 vpWidth vpHeight -- * 0.5
          }
        aTime = state / 3
        aCenter = tileSize * 8
        aRadius = tileSize * 2

        b = tmpBase
          { TileMap.tmpViewOffset =
              bCenter + vec2 (sin bTime) (cos bTime) * bRadius
          , TileMap.tmpViewportSize =
              vec2 vpWidth vpHeight * (2 + vec2 bZoom bZoom)
          }
        bTime = -state / 2
        bCenter = 0 -- tileSize * vec2 150 50 + vec2 (sin aTime) (cos aTime)
        bRadius = tileSize * 5
        bZoom = -1.0 -- sin (bTime / 5)

    tmpBase = TileMap.TileMapParams
      { tmpViewOffset         = 0
      , tmpViewportSize       = 1
      , tmpMapTextureSize     = textures.bitpack.layer0.size
      , tmpTilesetTextureSize = textures.bitpack.tileset.size
      , tmpTileSize           = tileSize
      , tmpTilesetOffset      = 0
      , tmpTilesetBorder      = 1
      , tmpTextureIds         = bitpackTextureIds
      }

    staticTransforms =
      [ Transform.translate (-0.4) 0.1 0.0 <>
        Transform.scaleXY vpWidth vpHeight

      , Transform.translate 0.4 (-0.1) 0.1 <>
        Transform.scaleXY vpWidth vpHeight
      ]
