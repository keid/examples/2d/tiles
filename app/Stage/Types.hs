module Stage.Types
  ( Stage
  , FrameResources(..)
  , RunState(..)
  ) where

import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0
import Render.Unlit.Textured.Model qualified as UnlitTextured
import Render.Unlit.TileMap.Model qualified as TileMap
import Resource.Buffer qualified as Buffer

import Stage.Resource.Texture.Flat qualified as Flat
import Stage.World.Viewport qualified as Viewport

type Stage = Basic.Stage FrameResources RunState

data FrameResources = FrameResources
  { frScene :: Set0.FrameResource '[Set0.Scene]

  , frTiles :: TileMap.ObserverCoherent
  }

data RunState = RunState
  { rsSceneP    :: Set0.Process
  , rsQuadUV    :: UnlitTextured.Model 'Buffer.Staged
  , rsTextures  :: Flat.TextureCollection
  , rsViewportP :: Viewport.Process
  }
